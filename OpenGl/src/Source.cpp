#include<GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#define ASSERT(x) if (!(x)) __debugbreak();
#define GLCall(x) GLClearError();\
	x;\
	ASSERT (GLlogCall(#x,__FILE__,__LINE__))
static void GLClearError()
{
	while (glGetError() != GL_NO_ERROR);
}
static bool GLlogCall(const char* function, const char* file, int line)
{
	while (GLenum error = glGetError())
	{
		std::cout << "file:"<<file<<"   function:"<<function<<"   line:"<<line<<"   error:: " << error << std::endl;
		return false;
	}
	return true;
}
struct ShaderProgSource {
	std::string vertexSource;
	std::string fragmentSource;
};
static ShaderProgSource paraseShader(const std::string filePath)
{
	enum class ShaderType
	{
		NONE =-1, VERTEX, FRAGMENT
	};
	std::ifstream stream(filePath);
	std::string line;
	std::stringstream ss[2];
	ShaderType type = ShaderType::NONE;
	if (stream.is_open())
	{

		
		while (getline(stream, line))
		{
			if (line.find("#shader") != std::string::npos)
			{
				if (line.find("Vertex") != std::string::npos)
				{
					type = ShaderType::VERTEX;
				}
				else if (line.find("Fragment") != std::string::npos)
				{
					type = ShaderType::FRAGMENT;
				}
			}
			else
			{
				ss[(int)type] << line << "\n";
			}
		}

		stream.close();
	}
	return { ss[0].str(), ss[1].str() };
}
static unsigned int compileShader(unsigned int type, const std::string source)
{
	unsigned int id = glCreateShader(type);
	const char* src = source.c_str();
	glShaderSource(id, 1, &src, nullptr);
	glCompileShader(id);
	int result;
	glGetShaderiv(id, GL_COMPILE_STATUS, &result);
	if (result == GL_FALSE)
	{
		std::cout << "error compiling"<<(type == GL_VERTEX_SHADER ?"vertex" : "frag") << " shader\n";
		int length;
		glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);
		char* message = (char*)alloca(length * sizeof(char));
		glGetShaderInfoLog(id, length, &length, message);
		std::cout << message << std::endl;
		glDeleteShader(id);
		return 0;
	}

	return id;
}
static int createShader(const std::string vertexShader, const std::string fragmentShader)
{
	unsigned int program = glCreateProgram();
	unsigned int vs = compileShader( GL_VERTEX_SHADER, vertexShader);
	unsigned int fs = compileShader(GL_FRAGMENT_SHADER, fragmentShader);
	
	glAttachShader(program, vs);
	glAttachShader(program, fs);
	glLinkProgram(program);
	glValidateProgram(program);


	glDeleteShader(vs);
	glDeleteShader(fs);
	return program;
}
int main(void)
{
	GLFWwindow* window;

	/* Initialize the library */
	if (!glfwInit())
		return -1;

	/* Create a windowed mode window and its OpenGL context */
	window = glfwCreateWindow(640, 480, "Hello World", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		return -1;
	}

	/* Make the window's context current */
	glfwMakeContextCurrent(window);

	glfwSwapInterval(1);

	if (glewInit() != GLEW_OK)
	{
		std::cout << "error!\n";
	}
	std::cout << glGetString(GL_VERSION) << std::endl;


	//vertex
	//using only the needed vetexes and then describong the data using the indices
	float positions[] = {
	    -0.5f,  -0.5f,
		 0.5f,  -0.5f,
		 0.5f,   0.5f,
		-0.5f,   0.5f
	};
	//describing the vertexes (deviding them to two shapes).
	unsigned int indices[]
	{
		0,1,2,
		2,3,0
	};



	unsigned int buffer;

	//creating and binding the buffer
	glGenBuffers(1, &buffer);
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	glBufferData(GL_ARRAY_BUFFER, 4 *2  * sizeof(float), positions, GL_STATIC_DRAW);

	//describing the data 
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2, 0);

	unsigned int ibo;
	//creating a index vuffer to describe the data above.
	glGenBuffers(1, &ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3 * 2 * sizeof(unsigned int), indices, GL_STATIC_DRAW);





	//adding the shaders we wrote in res/shaders/Basic.shader
	ShaderProgSource source = paraseShader("res/shaders/Basic.shader");
	/*std::cout << "vertex Source:" << std::endl;
	std::cout << source.vertexSource << std::endl;
	std::cout << "fragment Source:" << std::endl;
	std::cout << source.fragmentSource << std::endl;*/
	unsigned int shader = createShader(source.vertexSource, source.fragmentSource);
	glUseProgram(shader);
	int u_ColorId = glGetUniformLocation(shader, "u_Color");
	ASSERT(u_ColorId != -1);
	//GLCall(glUniform4f(u_ColorId, 0.2f, 0.3f, 0.8f, 1.0f));

	float r = 0.0;
	float inc = 0.05f;
	/* Loop until the user closes the window */
	while (!glfwWindowShouldClose(window))
	{
		/* Render here */
		glClear(GL_COLOR_BUFFER_BIT);
		
		//glDrawArrays(GL_TRIANGLES, 0, 6);
		
		GLCall(glUniform4f(u_ColorId, r, 0.3f, 0.8f, 1.0f));
		
		GLCall(glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr));
		





		if (r > 1.0f)
			inc = -0.05f;
		else if (r < 0.0f)
			inc = 0.05f;
		r += inc;
		/* Swap front and back buffers */
		glfwSwapBuffers(window);

		/* Poll for and process events */
		glfwPollEvents();
	}
	glDeleteProgram(shader);
	glfwTerminate();
	return 0;
}

